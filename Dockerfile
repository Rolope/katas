FROM node:14-alpine

COPY package.json yarn.lock /katas/

WORKDIR katas

RUN yarn install

COPY ./src src

CMD ["yarn", "test"]

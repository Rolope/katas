# CDojo
It's a repo for my personal kata practices with tdd
Code is implemented in ES6 javascript and tests run in jest

## Dependencies
- Docker

## Commands
- Run `make build` to create the image
- Run `make recreate` to force a rebuild of the image 
- Run `make shell` to jump into containers shell. There you can run the tests for each folder by running `yarn test -t <kata_name>`
- Run `make test` to run all the jest tests

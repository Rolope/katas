image_name = node14-alpine
container_name = katas

build:
	docker build -t ${image_name} .

recreate:
	docker build -t ${image_name} . --no-cache
	
shell: 
	docker run -it -v $(shell pwd):/${container_name} --rm --name ${container_name} ${image_name} sh

test:
	docker run -v $(shell pwd):/${container_name} --rm --name ${container_name} ${image_name}
